Name:           gnome-tweaks
Version:        42.0
Release:        1
Summary:        Previously known as Tweak Tool. Graphical interface for advanced GNOME 3 settings.
License:        GPLv3 and CC0
URL:            https://wiki.gnome.org/Apps/Tweaks
Source0:        https://download.gnome.org/sources/%{name}/42/%{name}-%{version}.tar.xz
BuildArch:      noarch

BuildRequires:  meson desktop-file-utils libappstream-glib gettext
BuildRequires:  pkgconfig(libhandy-1) python3-devel

Requires:       python3 python3-gobject >= 3.10 gnome-settings-daemon sound-theme-freedesktop
Requires:	glib2 >= 2.58 gtk3 >= 3.12 gnome-desktop3 >= 3.30 libhandy >= 1.0  libsoup libnotify
Requires:	pango gsettings-desktop-schemas >= 3.33.0 gnome-shell >= 3.24 mutter gnome-software
Requires:	nautilus gnome-shell-extension-desktop-icons

Provides:       gnome-tweak-tool = %{version}-%{release}

%description
GNOME Tweaks allows customizing looks, behavior or settings of your desktop in GNOME 3.
The concrete settings inclute fonts, themes, colors, windows and so on.
Tweaks is designed for GNOME Shell but can be used in other desktops.
A few features will be missing when Tweaks is run on a different desktop.

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

%find_lang %{name}

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop
appstream-util validate-relax --nonet $RPM_BUILD_ROOT/%{_datadir}/metainfo/*.appdata.xml

%files -f %{name}.lang
%doc AUTHORS NEWS README.md
%license LICENSES/*
%{_bindir}/gnome-tweaks
%{python3_sitelib}/gtweak/
%{_libexecdir}/gnome-tweak-tool-lid-inhibitor
%{_datadir}/applications/*.desktop
%{_datadir}/gnome-tweaks/*
%{_datadir}/icons/hicolor/*/apps/org.gnome.tweaks.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.tweaks-symbolic.svg
%{_datadir}/metainfo/*.appdata.xml
%{_datadir}/glib-2.0/schemas/*.xml

%changelog
* Thu Nov 23 2023 lwg <liweiganga@uniontech.com> - 42.0-1
- update to version 42.0

* Mon Apr 18 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.34.0-2
- Add gnome-tweaks.yaml

* Mon Oct 18 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.34.0-1
- Package gnome-tweaks init with version 3.34.0

* Thu Nov 14 2019 wangye<wangye54@huawei.com> - 3.30.2-3
- Package init
